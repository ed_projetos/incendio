#ifndef MATRIZVIEW
#define MATRIZVIEW
#include <SFML/Graphics.hpp>
#include "matriz/stateview.h"
#include "matriz/matriz.h"
#include <queue>
#include <sstream>

using namespace std;
using namespace sf;

class MatrizView{

private:
    //Color _color_back;
    //Color _color_front;

    std::shared_ptr<sf::RenderWindow> _janela;
    StateView _sview;
    Pincel _pincel;

    struct Offset{
        int left;
        int right;
        int up;
        int down;
    };

    Offset off;


public:
    MatrizView():
        MatrizView(std::shared_ptr<sf::RenderWindow>(new RenderWindow(VideoMode(1000, 800), "Janelinha")))
    {}

    MatrizView(std::shared_ptr<sf::RenderWindow> janela):
        _janela(janela),
        _sview(janela),
        _pincel(janela)
    {
        //_color_back = sf::Color(216,216,191);
        _sview.setColorBack(sf::Color(216,216,191));
        _sview.setColorFront(sf::Color::Black);
        //_color_front = sf::Color::Black;
        off.up = _sview.getOffsetUp();
        off.left = 30;
        off.right = 30;
        off.down = 30;
        _sview.setSync(true);
        _sview.init();
    }

    //envia um estado para o gerenciador de estados
    void push(const matriz<char>& mat){
        auto f = bind(&MatrizView::draw, this, mat);
        _sview.push(f);
    }

    void wait(){
        _sview.wait();
    }
    
    //mostra um texto e retorna um ponto clicado para o usuario
    Vector2u selectPoint(const matriz<char>& mat, const string & texto){
        int linhas = mat.sizeY();
        int colunas = mat.sizeX();
        bool acabou = false;
        while(!acabou){
            sf::Event event;
            float lado = getSide(linhas, colunas);
            while(_janela->pollEvent(event)){
                if(event.type == sf::Event::Closed){
                    _janela->close();
                    acabou = true;
                }
                else if (event.type == sf::Event::Resized){
                    //evitar amassamento da imagem
                    _janela->setView(sf::View(
                        sf::FloatRect(0, 0, event.size.width, event.size.height)));
                }
                else if(event.type == sf::Event::KeyPressed){
                    if(event.key.code == sf::Keyboard::Q)
                        acabou = true;
                }
                else if(event.type == sf::Event::MouseButtonPressed){
                    if(event.mouseButton.button == sf::Mouse::Left){
                        Vector2u square;
                        int px = event.mouseButton.x;
                        int py = event.mouseButton.y;
                        Vector2i mouse(px, py);

                        if(getValidSquare(square, mouse, linhas, colunas, lado))
                            return square;
                    }
                }
            }


            _janela->clear();
            _pincel.drawText(Vector2f(off.left, 0), texto, _sview.getColorFront());
            this->draw(mat);
            _janela->display();
        }
        return Vector2u(0,0);
    }
    
    //permite ao usuario desenhar uma matriz. Salva o desenho em mat
    void paint(matriz<char> &mat){
        char cor_atual = 'k';
        bool acabou = false;
        while(!acabou){
            sf::Event event;
            float lado = getSide(mat.sizeY(), mat.sizeX());
            while(_janela->pollEvent(event)){
                if(event.type == sf::Event::Closed){
                    _janela->close();
                    acabou = true;
                }
                else if (event.type == sf::Event::Resized){
                    //evitar amassamento da imagem
                    _janela->setView(sf::View(
                            sf::FloatRect(0, 0, event.size.width, event.size.height)));
                }
                else if(event.type == sf::Event::KeyPressed){
                    if(event.key.code == sf::Keyboard::Q)
                        acabou = true;
                    else
                        ColorMap::updateColor(event.key.code, &cor_atual);
                }
            }

            //processar cliques no mouse de pinturas
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
                Vector2u square;
                Vector2i mouse = sf::Mouse::getPosition(*_janela);
                if(getValidSquare(square, mouse, mat.sizeY(), mat.sizeX(), lado))
                    mat.get(square.y,square.x) = cor_atual;
            }
            _janela->clear(_sview.getColorBack());
            string texto = "Digite " + ColorMap::getColorList() + " para trocar de Cor " +
                    "ou Q para terminar!";
            _pincel.drawText(Vector2f(off.left, 0), texto, _sview.getColorFront());
            //this->printColors(0, 40, cor_atual);
            this->draw(mat);
            _janela->display();
        }
    }

private:

    //retorna o lado do quadrado para o tamanho de tela atual
    float getSide(int linhas, int colunas){
        Vector2u dim = _janela->getSize();
        //cout <<"janela " << dim.x << " " << dim.y << endl;
        float propTela = dim.x/(float)dim.y;
        float propMat = colunas/(float)linhas;
        float lado = 0;
        if(propTela > propMat)
            lado = (dim.y - off.up - off.down)/(float)linhas;
        else
            lado = (dim.x - off.left - off.right)/(float)colunas;
        return lado;
    }

    //desenha a matriz na tela usando o mapa de cores
    void draw(const matriz<char>& mat){
        int linhas = mat.sizeY();
        int colunas = mat.sizeX();
        float side = getSide(mat.sizeY(), mat.sizeX());
        RectangleShape rect;
        int larg = 1;
        rect.setSize(Vector2f(side - larg, side - larg));
        //print squares
        for(int x = 0; x < colunas; x++){
            for(int y = 0; y < linhas; y++){
                rect.setFillColor(ColorMap::getColor(mat.get(y,x)));
                rect.setPosition(larg + off.left + x * side, larg + off.up + y * side);
                _janela->draw(rect);
            }
        }
    }

    //dadas as configuracoes de offset, verificar qual posicao da matriz foi clicada
    bool getValidSquare(Vector2u &square, Vector2i &pos, int linhas, int colunas, float lado){
        int y = (pos.y - off.up) / lado;
        int x = (pos.x - off.left) / lado;
        if(y >= 0 and y < linhas){
            if(x >=0 and x < colunas){
                square.x = x;
                square.y = y;
                return true;
            }
        }
        return false;
    }

};

#endif // MATRIZVIEW

