#include <iostream>
#include <fstream>
#include <cstdlib>
#include "matrizview.h"

static std::shared_ptr<sf::RenderWindow> janela(new sf::RenderWindow(sf::VideoMode(860, 645), L"Incêndio"));
static MatrizView view(janela);
static matriz<char> cenario;

// Queremos queimar a arvore na linha e coluna passadas (dentro do cenario),
// propagando o incendio as arvores vizinhas, e no final contar quantas
// arvores foram queimadas no total.
unsigned int queimar(unsigned int lin, unsigned int col) {
    return 0;
}

int main() {
    std::ifstream arquivo;

    // Selecionar arquivo de cenario e ler diretamente em matriz
    arquivo.open("input/cenario-1.in");

    if(!arquivo) {
        std::cerr << "Erro ao abrir arquivo" << std::endl;
        return EXIT_FAILURE;
    }

    arquivo >> cenario;

    // Indicar o estado atual do cenario no visualizador
    view.push(cenario);

    std::cout << queimar(7, 3) << " arvores foram queimadas." << std::endl;

    // Esperar a janela ser fechada
    view.wait();

    return EXIT_SUCCESS;
}
