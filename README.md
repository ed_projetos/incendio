![dfgfhff.png](https://bitbucket.org/repo/6dKBKj/images/3714505993-dfgfhff.jpg)
# Incêncio Florestal #

Créditos:
* Prof. David Sena ([sena&#46;ufc&commat;g&#109;&#97;il&#46;com](mailto:sena&#46;ufc&commat;g&#109;&#97;il&#46;com))
* Prof. Arthur Araruna ([ararunaufc&commat;g&#109;&#97;il&#46;com](mailto:ararunaufc&commat;g&#109;&#97;il&#46;com))

## 1 - Instruções Gerais ##
O objetivo dessa atividade é determinar quantas e quais árvores serão queimadas caso seja iniciado um incêndio. A visualização é dada por uma matriz de cores.

![cenario-verde.png](http://s19.postimg.org/6k4lib6bn/Captura_de_tela_de_2015_11_09_15_07_04.png) ![cenario-queimado.png](http://s19.postimg.org/6f0u8mz0j/Captura_de_tela_de_2015_11_09_15_33_49.png)

### 1.1 - Descrição do Problema ###
O cenário pode ser visto como uma matriz _m_ x _n_, tal que _m_ = número de linhas e _n_ = número de colunas e que em cada posição `(linha,coluna)` podemos encontrar uma árvore viva (cor verde), uma árvore queimada (cor vermelha) ou nada (cor branca). O usuário deve informar duas posições dessa matriz, e o programa deve então mostrar as árvores que seriam queimadas a partir daquela em um incêndio, além de contar a quantidade.

### 1.2 - Regras de propagação do incêndio ###
O incêndio inicia da árvore determinada pelo usuário, queimando-a, e se propaga de uma árvore queimada para as árvores vivas que estejam imediatamente acima, abaixo ou nas laterais dela. O incêndio não se propaga diagonalmente nem contamina áreas vazias.

Assim, se em algum momento uma árvore for queimada, as suas vizinhas (exceto as em diagonal) também serão queimadas e, por sua vez, queimarão suas próprias vizinhas. O incêndio pára de se propagar no momento em que não há mais árvores a queimar segundo as regras anteriores.

## 2 - Implementação ##
Seu objetivo é implementar a visualização do incêndio se propagando. Ou seja, sempre que determinar que uma árvore será queimada, ela deve trocar de cor. Cria-se uma matriz _m_ x _n_, dados que podem ser informados pelo o usuário, onde _m_ é a altura, ou seja, a quantidade de linhas e _n_ a largura, a quantidade de colunas.
Receba uma posição válida pra começar a percorrer a matriz, determinando quais árvores serão queimadas e calculando quantas queimaram. A melhor forma de resolver o problema é através de um algoritmo recursivo.

### 2.1 - Exemplo ###
O programa deve criar uma matriz _m_ x _n_ e solicitar ao usuário que informe uma posição válida pra começar a preencher a matriz. Abaixo um exemplo com uma matriz _10_ x _10_ iniciando da posição `(8, 4)`.

Na primeira iteração, o usuário escolheu uma posição válida. O programa, então, marca a árvore como queimada, representado pela cor vermelha:

![ptela.png](http://s19.postimg.org/x3x6kg6v7/Captura_de_tela_de_2015_11_09_15_24_45.png)

Então, percorrendo a matriz, propagamos o incêndio às árvores que serão queimadas. Abaixo, o estado após algumas iterações:

![stela.png](http://s19.postimg.org/6f0u8mz0j/Captura_de_tela_de_2015_11_09_15_33_49.png)

A segunda parte é concluida e mostra resultado final na tela, exibindo também a quantidade de árvores queimadas:

![ttela.png](http://s19.postimg.org/ox56svgsj/Captura_de_tela_de_2015_11_09_15_24_49.png) ![qtela.png](http://s19.postimg.org/xe4p3slhf/Captura_de_tela_de_2015_11_09_15_30_28.png)

Finalizou, sair liberando todas as memórias alocadas.
