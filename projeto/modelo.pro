TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += c++11

SOURCES += \
    matriz/stateview.cpp \
    aluno/main.cpp

HEADERS += \
    matriz/matriz.h \
    matriz/stateview.h \
    matrizview.h

LIBS += -lpthread -lsfml-graphics -lsfml-window -lsfml-system

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

OTHER_FILES += input/cenario-1.in

CONFIG(release, debug|release) {
    DESTDIR = release
} else {
    DESTDIR = debug
}

copyfiles.commands += @echo "NOW COPYING ADDITIONAL FILES" &
copyfiles.commands += cp -r ../projeto/input $${DESTDIR}/

QMAKE_EXTRA_TARGETS += copyfiles
POST_TARGETDEPS += copyfiles
